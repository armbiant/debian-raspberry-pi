# Debian Rpi
This repository is a close, though not exact, clone of
the [Debian Raspberry Pi Image Spec](https://salsa.debian.org/raspi-team/image-specs) system.

## Name
Open Tasmania Debian RPi

## Description
At this point in time it not presented as a diff, nor as a fork, so as to preserve code independence between
the two while both are under development. For the vast majority of use cases, please consider simply using upstream.

## Badges
[![pipeline status](https://gitlab.com/opentasmania/debian-rpi/badges/main/pipeline.svg)](https://gitlab.com/opentasmania/debian-rpi/-/commits/main)
<!-- [![coverage report](https://gitlab.com/opentasmania/debian-rpi/badges/main/coverage.svg)](https://gitlab.com/opentasmania/debian-rpi/-/commits/main) -->
[![Latest Release](https://gitlab.com/opentasmania/debian-rpi/-/badges/release.svg)](https://gitlab.com/opentasmania/debian-rpi/-/releases)

## Installation
Please refer to the [upstream readme](https://salsa.debian.org/raspi-team/image-specs/-/blob/master/README.md) for
detailed info requirements.

## Usage
Adjust the [Makefile](Makefile)'s BUILD_FAMILIES and BUILD_RELEASES as needed. Then follow the relevant section/s of
the [upstream readme](https://salsa.debian.org/raspi-team/image-specs/-/blob/master/README.md) for detailed info
requirements.

## Support
Please feel free to [file an issue](https://gitlab.com/opentasmania/debian-rpi/-/issues) on the Gitlab page. 
## Roadmap
* Default user setup
* WiFi setup
* Simple security setup
* Locales set to Tasmania
* Various custom built images such as
** Home Assistant
** Microk8s
** OpnSense
** Pimox
** Gitlab
** Kodi
** Kali
** TAILS(?)
** ___YOUR IDEA HERE___

## Contributing
YOUR NICK HERE

## Authors and acknowledgment
Original by Michael Stapelberg. This variant Peter Lawler.

## License
The 3-Clause BSD License. See the [LICENCE](license) file for details.

## Project status
Mostly harmless