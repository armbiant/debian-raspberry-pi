#!/usr/bin/python3

# Tidied* by Pete Lawler

import logging
import re
import subprocess
import sys

logging.basicConfig(level=logging.WARNING, format='%(levelname)s: %(message)s')

# pylint: disable=invalid-name

# Constants
VALID_VERSIONS = ["1", "2", "3", "4"]
VALID_SUITES = ['buster', 'bullseye', 'bookworm']

### Sanity/usage checks

if len(sys.argv) != 3:
    logging.error("E: need 2 arguments")
    sys.exit(1)

raspberry_pi_version = sys.argv[1]
if raspberry_pi_version not in VALID_VERSIONS:
    logging.error(f"E: unsupported raspberry pi version {raspberry_pi_version}")
    sys.exit(1)

distribution_suite = sys.argv[2]
if distribution_suite not in VALID_SUITES:
    logging.error(f"E: unsupported distribution suite {distribution_suite}")
    sys.exit(1)

target_yaml = f'raspi_{raspberry_pi_version}_{distribution_suite}.yaml'

## Setting variables based on suite and version starts here

# Arch, kernel, DTB
version_map = {
    "1": {"arch": "armel", "linux": "linux-image-rpi", "dtb": "/usr/lib/linux-image-*-rpi/bcm*rpi-*.dtb"},
    "2": {"arch": "armhf", "linux": "linux-image-armmp", "dtb": "/usr/lib/linux-image-*-armmp/bcm*rpi*.dtb"},
    "3": {"arch": "arm64", "linux": "linux-image-arm64", "dtb": "/usr/lib/linux-image-*-arm64/broadcom/bcm*rpi*.dtb"},
    "4": {"arch": "arm64", "linux": "linux-image-arm64", "dtb": "/usr/lib/linux-image-*-arm64/broadcom/bcm*rpi*.dtb"},
}

# APT and default firmware (name + handling)
suite_map = {
    "buster": {"security_suite": "buster/updates", "raspi_firmware": "raspi3-firmware", "fix_firmware": True},
    "bullseye": {"security_suite": "bullseye-security", "raspi_firmware": "raspi-firmware", "fix_firmware": False},
    "bookworm": {"security_suite": "bookworm-security", "raspi_firmware": "raspi-firmware", "fix_firmware": False},
}
# Bookworm introduced the 'non-free-firmware' component¹; before that,
# raspi-firmware was in 'non-free'
#
# ¹ https://www.debian.org/vote/2022/vote_003
firmware_component_map = {
    'bookworm': ('non-free-firmware', 'non-free'),
    None: ('non-free', '')
}

wireless_firmware_map = {
    '2': ('', ''),
    'other': ('firmware-brcm80211', 'bluez-firmware')
}

serial_map = {
    '1': 'ttyAMA0,115200',
    '2': 'ttyAMA0,115200',
    '3': 'ttyS1,115200',
    '4': 'ttyS1,115200',
}

extra_root_shell_cmds_map = {}
external_root_shell_cmds_map = {}
extra_chroot_shell_cmds_map = {
    '4': [
        "sed --in-place 's/cma=64M //' /boot/firmware/cmdline.txt",
    ],
}
external_chroot_shell_cmds_map = {}
# XXX: The disparity between suite seems to be a bug, pick a naming
# and stick to it!
#
# Hostname:
hostname_map = {
    'buster': 'rpi{}'.format(raspberry_pi_version),
    '*': 'rpi_{}'.format(raspberry_pi_version)
}
# Buster requires an existing, empty /etc/machine-id file:
distribution_options = {
    'buster': ('touch /etc/machine-id', 'systemd'),
    'default': ('echo "uninitialized" > /etc/machine-id', 'systemd-timesyncd')
}
arch = version_map[raspberry_pi_version]["arch"]
linux = version_map[raspberry_pi_version]["linux"]
dtb = version_map[raspberry_pi_version]["dtb"]
security_suite = suite_map[distribution_suite]["security_suite"]
raspi_firmware = suite_map[distribution_suite]["raspi_firmware"]
fix_firmware = suite_map[distribution_suite]["fix_firmware"]
firmware_component, firmware_component_old = firmware_component_map.get(distribution_suite,
                                                                        firmware_component_map[None])
wireless_firmware, bluetooth_firmware = wireless_firmware_map.get(raspberry_pi_version, wireless_firmware_map['other'])
serial = serial_map.get(raspberry_pi_version, '')
hostname = hostname_map.get(distribution_suite, hostname_map['*'])
touch_machine_id, systemd_timesyncd = distribution_options.get(distribution_suite, distribution_options['default'])

# Nothing yet!
extra_root_shell_cmds = extra_root_shell_cmds_map.get(raspberry_pi_version, [])
external_root_shell_cmds = external_root_shell_cmds_map.get(raspberry_pi_version, [])
extra_chroot_shell_cmds = extra_chroot_shell_cmds_map.get(raspberry_pi_version, [])
external_chroot_shell_cmds = external_chroot_shell_cmds_map.get(raspberry_pi_version, [])

### The following prepares substitutions based on variables set earlier

# Commands to fix the firmware name in the systemd unit:
fix_firmware_cmds = [
    'sed --in-place s/raspi-firmware/raspi3-firmware/ ${ROOT?}/etc/systemd/system/rpi-reconfigure-raspi-firmware.service'] if fix_firmware else []

# TODO: There has to be a cleaner way to do this
# Pi 4 on buster requires some backports:
backports_enable = False
backports_suite = '%s-backports' % distribution_suite
if raspberry_pi_version == '4' and distribution_suite == 'buster':
    backports_enable = "# raspi 4 needs kernel and firmware newer than buster's"
    linux = '%s/%s' % (linux, backports_suite)
    raspi_firmware = 'raspi-firmware/%s' % backports_suite
    wireless_firmware = 'firmware-brcm80211/%s' % backports_suite
    fix_firmware = False

if raspberry_pi_version == '3' and distribution_suite == 'buster':
    backports_enable = "# raspi 3 needs firmware-brcm80211 newer than buster's for wifi"
    wireless_firmware = 'firmware-brcm80211/%s' % backports_suite

# Enable backports with a reason, or add commented-out entry:
if backports_enable:
    backports_stanza = """
%s
deb http://deb.debian.org/debian/ %s main %s
""" % (backports_enable, backports_suite, firmware_component)
else:
    # ugh
    backports_stanza = """
# Backports are _not_ enabled by default.
# Enable them by uncommenting the following line:
# deb http://deb.debian.org/debian %s main %s
""" % (backports_suite, firmware_component)

gitcommit = subprocess.getoutput("git show -s --pretty='format:%C(auto)%h (%s, %ad)' --date=short ")
buildtime = subprocess.getoutput("date --utc +'%Y-%m-%d %H:%M'")


### Write results:

def align_replace(text, pattern, replacement):
    """
    This helper lets us keep the indentation of the matched pattern
    with the upcoming replacement, across multiple lines. Naive
    implementation, please make it more pythonic!
    """
    lines = text.splitlines()
    pattern = r'^(\s+)' + pattern
    for i, line in enumerate(lines):
        m = re.match(pattern, line)
        if m:
            indent = m.group(1)
            del lines[i]
            lines[i:i] = [f"{indent}{r}" for r in replacement]
            break
    return '\n'.join(lines) + '\n'


def main():
    with open('raspi_master.yaml', 'r') as in_file:
        with open(target_yaml, 'w') as out_file:
            in_text = in_file.read()
            out_replacements = [
                ('__RELEASE__', distribution_suite),
                ('__ARCH__', arch),
                ('__FIRMWARE_COMPONENT__', firmware_component),
                ('__FIRMWARE_COMPONENT_OLD__', firmware_component_old),
                ('__LINUX_IMAGE__', linux),
                ('__DTB__', dtb),
                ('__SECURITY_SUITE__', security_suite),
                ('__SYSTEMD_TIMESYNCD__', systemd_timesyncd),
                ('__RASPI_FIRMWARE__', raspi_firmware),
                ('__WIRELESS_FIRMWARE__', wireless_firmware),
                ('__BLUETOOTH_FIRMWARE__', bluetooth_firmware),
                ('__SERIAL_CONSOLE__', serial),
                ('__HOST__', hostname),
                ('__TOUCH_MACHINE_ID__', touch_machine_id),
                ('__GITCOMMIT__', gitcommit),
                ('__BUILDTIME__', buildtime)
            ]
            out_text = in_text
            for placeholder, value in out_replacements:
                out_text = out_text.replace(placeholder, value)

            align_replacements = [
                ('__FIX_FIRMWARE_PKG_NAME__', fix_firmware_cmds),
                ('__EXTRA_ROOT_SHELL_CMDS__', extra_root_shell_cmds),
                ('__EXTERNAL_ROOT_SHELL_CMDS__', external_root_shell_cmds),
                ('__EXTRA_CHROOT_SHELL_CMDS__', extra_chroot_shell_cmds),
                ('__EXTERNAL_CHROOT_SHELL_CMDS__', external_chroot_shell_cmds),
                ('__BACKPORTS__', backports_stanza.splitlines())
            ]
            for placeholder, value in align_replacements:
                out_text = align_replace(out_text, placeholder, value)

            filtered = [x for x in out_text.splitlines() if not x.strip().endswith('-')]
            out_file.write('\n'.join(filtered) + "\n")


if __name__ == "__main__":
    main()
